package controllers

import play.api.mvc._

object Application extends Controller {
  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def bs3() = Action {
    Ok(views.html.docs.bs3example())
  }

  def cssDoc(topic: String) = Action {
    def view = topic match {
      case "gridsystem" => views.html.docs.css.docGridsystem()
    }
    Ok(view)
  }

  def componentDoc(topic: String) = Action {
    def view = topic match {
      case "glyphicons" => views.html.docs.components.docGlyphicons()
      case "dropdowns" => views.html.docs.components.docDropdowns()
      case "buttongroups" => views.html.docs.components.docButtongroups()
      case "buttondropdowns" => views.html.docs.components.docButtondropdowns()
      case "navs" => views.html.docs.components.docNavs()
      case "navbar" => views.html.docs.components.docNavbar()
      case "breadcrumbs" => views.html.docs.components.docBreadcrumbs()
      case "pagination" => views.html.docs.components.docPagination()
      case "labels" => views.html.docs.components.docLabels()
      case "badges" => views.html.docs.components.docBadges()
      case "jumbotron" => views.html.docs.components.docJumbotron()
      case "progressbars" => views.html.docs.components.docProgressbars()
      case "mediaobject" => views.html.docs.components.docMediaobject()
      case "listgroup" => views.html.docs.components.docListgroup()
      case "panels" => views.html.docs.components.docPanels()
      case "alerts" => views.html.docs.components.docAlerts()
      case "wells" => views.html.docs.components.docWells()
    }
    Ok(view)
  }

  def javascriptDoc(topic: String) = Action {
    def view = topic match {
      case "tabs" => views.html.docs.javascript.docTab()
      case "tooltip" => views.html.docs.javascript.docTooltip()
      case "popover" => views.html.docs.javascript.docPopover()
      case "collapse" => views.html.docs.javascript.docCollapse()
      case "carousel" => views.html.docs.javascript.docCarousel()
    }
    Ok(view)
  }
}