package views.bs3

import play.twirl.api.Html

object glyphicon {
  def asterisk(description: String = "") = asteriskIcon(description)

  def plus(description: String = "") = plusIcon(description)

  def euro(description: String = "") = euroIcon(description)

  def eur(description: String = "") = eurIcon(description)

  def minus(description: String = "") = minusIcon(description)

  def cloud(description: String = "") = cloudIcon(description)

  def envelope(description: String = "") = envelopeIcon(description)

  def pencil(description: String = "") = pencilIcon(description)

  def glass(description: String = "") = glassIcon(description)

  def music(description: String = "") = musicIcon(description)

  def search(description: String = "") = searchIcon(description)

  def heart(description: String = "") = heartIcon(description)

  def star(description: String = "") = starIcon(description)

  def starEmpty(description: String = "") = starEmptyIcon(description)

  def user(description: String = "") = userIcon(description)

  def film(description: String = "") = filmIcon(description)

  def thLarge(description: String = "") = thLargeIcon(description)

  def th(description: String = "") = thIcon(description)

  def thList(description: String = "") = thListIcon(description)

  def ok(description: String = "") = okIcon(description)

  def remove(description: String = "") = removeIcon(description)

  def zoomIn(description: String = "") = zoomInIcon(description)

  def zoomOut(description: String = "") = zoomOutIcon(description)

  def off(description: String = "") = offIcon(description)

  def signal(description: String = "") = signalIcon(description)

  def cog(description: String = "") = cogIcon(description)

  def trash(description: String = "") = trashIcon(description)

  def home(description: String = "") = homeIcon(description)

  def file(description: String = "") = fileIcon(description)

  def time(description: String = "") = timeIcon(description)

  def road(description: String = "") = roadIcon(description)

  def downloadAlt(description: String = "") = downloadAltIcon(description)

  def download(description: String = "") = downloadIcon(description)

  def upload(description: String = "") = uploadIcon(description)

  def inbox(description: String = "") = inboxIcon(description)

  def playCircle(description: String = "") = playCircleIcon(description)

  def repeat(description: String = "") = repeatIcon(description)

  def refresh(description: String = "") = refreshIcon(description)

  def listAlt(description: String = "") = listAltIcon(description)

  def lock(description: String = "") = lockIcon(description)

  def flag(description: String = "") = flagIcon(description)

  def headphones(description: String = "") = headphonesIcon(description)

  def volumeOff(description: String = "") = volumeOffIcon(description)

  def volumeDown(description: String = "") = volumeDownIcon(description)

  def volumeUp(description: String = "") = volumeUpIcon(description)

  def qrcode(description: String = "") = qrcodeIcon(description)

  def barcode(description: String = "") = barcodeIcon(description)

  def tag(description: String = "") = tagIcon(description)

  def tags(description: String = "") = tagsIcon(description)

  def book(description: String = "") = bookIcon(description)

  def bookmark(description: String = "") = bookmarkIcon(description)

  def print(description: String = "") = printIcon(description)

  def camera(description: String = "") = cameraIcon(description)

  def font(description: String = "") = fontIcon(description)

  def bold(description: String = "") = boldIcon(description)

  def italic(description: String = "") = italicIcon(description)

  def textHeight(description: String = "") = textHeightIcon(description)

  def textWidth(description: String = "") = textWidthIcon(description)

  def alignLeft(description: String = "") = alignLeftIcon(description)

  def alignCenter(description: String = "") = alignCenterIcon(description)

  def alignRight(description: String = "") = alignRightIcon(description)

  def alignJustify(description: String = "") = alignJustifyIcon(description)

  def list(description: String = "") = listIcon(description)

  def indentLeft(description: String = "") = indentLeftIcon(description)

  def indentRight(description: String = "") = indentRightIcon(description)

  def facetimeVideo(description: String = "") = facetimeVideoIcon(description)

  def picture(description: String = "") = pictureIcon(description)

  def mapMarker(description: String = "") = mapMarkerIcon(description)

  def adjust(description: String = "") = adjustIcon(description)

  def tint(description: String = "") = tintIcon(description)

  def edit(description: String = "") = editIcon(description)

  def share(description: String = "") = shareIcon(description)

  def check(description: String = "") = checkIcon(description)

  def move(description: String = "") = moveIcon(description)

  def stepBackward(description: String = "") = stepBackwardIcon(description)

  def fastBackward(description: String = "") = fastBackwardIcon(description)

  def backward(description: String = "") = backwardIcon(description)

  def play(description: String = "") = playIcon(description)

  def pause(description: String = "") = pauseIcon(description)

  def stop(description: String = "") = stopIcon(description)

  def forward(description: String = "") = forwardIcon(description)

  def fastForward(description: String = "") = fastForwardIcon(description)

  def stepForward(description: String = "") = stepForwardIcon(description)

  def eject(description: String = "") = ejectIcon(description)

  def chevronLeft(description: String = "") = chevronLeftIcon(description)

  def chevronRight(description: String = "") = chevronRightIcon(description)

  def plusSign(description: String = "") = plusSignIcon(description)

  def minusSign(description: String = "") = minusSignIcon(description)

  def removeSign(description: String = "") = removeSignIcon(description)

  def okSign(description: String = "") = okSignIcon(description)

  def questionSign(description: String = "") = questionSignIcon(description)

  def infoSign(description: String = "") = infoSignIcon(description)

  def screenshot(description: String = "") = screenshotIcon(description)

  def removeCircle(description: String = "") = removeCircleIcon(description)

  def okCircle(description: String = "") = okCircleIcon(description)

  def banCircle(description: String = "") = banCircleIcon(description)

  def arrowLeft(description: String = "") = arrowLeftIcon(description)

  def arrowRight(description: String = "") = arrowRightIcon(description)

  def arrowUp(description: String = "") = arrowUpIcon(description)

  def arrowDown(description: String = "") = arrowDownIcon(description)

  def shareAlt(description: String = "") = shareAltIcon(description)

  def resizeFull(description: String = "") = resizeFullIcon(description)

  def resizeSmall(description: String = "") = resizeSmallIcon(description)

  def exclamationSign(description: String = "") = exclamationSignIcon(description)

  def gift(description: String = "") = giftIcon(description)

  def leaf(description: String = "") = leafIcon(description)

  def fire(description: String = "") = fireIcon(description)

  def eyeOpen(description: String = "") = eyeOpenIcon(description)

  def eyeClose(description: String = "") = eyeCloseIcon(description)

  def warningSign(description: String = "") = warningSignIcon(description)

  def plane(description: String = "") = planeIcon(description)

  def calendar(description: String = "") = calendarIcon(description)

  def random(description: String = "") = randomIcon(description)

  def comment(description: String = "") = commentIcon(description)

  def magnet(description: String = "") = magnetIcon(description)

  def chevronUp(description: String = "") = chevronUpIcon(description)

  def chevronDown(description: String = "") = chevronDownIcon(description)

  def retweet(description: String = "") = retweetIcon(description)

  def shoppingCart(description: String = "") = shoppingCartIcon(description)

  def folderClose(description: String = "") = folderCloseIcon(description)

  def folderOpen(description: String = "") = folderOpenIcon(description)

  def resizeVertical(description: String = "") = resizeVerticalIcon(description)

  def resizeHorizontal(description: String = "") = resizeHorizontalIcon(description)

  def hdd(description: String = "") = hddIcon(description)

  def bullhorn(description: String = "") = bullhornIcon(description)

  def bell(description: String = "") = bellIcon(description)

  def certificate(description: String = "") = certificateIcon(description)

  def thumbsUp(description: String = "") = thumbsUpIcon(description)

  def thumbsDown(description: String = "") = thumbsDownIcon(description)

  def handRight(description: String = "") = handRightIcon(description)

  def handLeft(description: String = "") = handLeftIcon(description)

  def handUp(description: String = "") = handUpIcon(description)

  def handDown(description: String = "") = handDownIcon(description)

  def circleArrowRight(description: String = "") = circleArrowRightIcon(description)

  def circleArrowLeft(description: String = "") = circleArrowLeftIcon(description)

  def circleArrowUp(description: String = "") = circleArrowUpIcon(description)

  def circleArrowDown(description: String = "") = circleArrowDownIcon(description)

  def globe(description: String = "") = globeIcon(description)

  def wrench(description: String = "") = wrenchIcon(description)

  def tasks(description: String = "") = tasksIcon(description)

  def filter(description: String = "") = filterIcon(description)

  def briefcase(description: String = "") = briefcaseIcon(description)

  def fullscreen(description: String = "") = fullscreenIcon(description)

  def dashboard(description: String = "") = dashboardIcon(description)

  def paperclip(description: String = "") = paperclipIcon(description)

  def heartEmpty(description: String = "") = heartEmptyIcon(description)

  def link(description: String = "") = linkIcon(description)

  def phone(description: String = "") = phoneIcon(description)

  def pushpin(description: String = "") = pushpinIcon(description)

  def usd(description: String = "") = usdIcon(description)

  def gbp(description: String = "") = gbpIcon(description)

  def sort(description: String = "") = sortIcon(description)

  def sortByAlphabet(description: String = "") = sortByAlphabetIcon(description)

  def sortByAlphabetAlt(description: String = "") = sortByAlphabetAltIcon(description)

  def sortByOrder(description: String = "") = sortByOrderIcon(description)

  def sortByOrderAlt(description: String = "") = sortByOrderAltIcon(description)

  def sortByAttributes(description: String = "") = sortByAttributesIcon(description)

  def sortByAttributesAlt(description: String = "") = sortByAttributesAltIcon(description)

  def unchecked(description: String = "") = uncheckedIcon(description)

  def expand(description: String = "") = expandIcon(description)

  def collapseDown(description: String = "") = collapseDownIcon(description)

  def collapseUp(description: String = "") = collapseUpIcon(description)

  def logIn(description: String = "") = logInIcon(description)

  def flash(description: String = "") = flashIcon(description)

  def logOut(description: String = "") = logOutIcon(description)

  def newWindow(description: String = "") = newWindowIcon(description)

  def record(description: String = "") = recordIcon(description)

  def save(description: String = "") = saveIcon(description)

  def open(description: String = "") = openIcon(description)

  def saved(description: String = "") = savedIcon(description)

  def _import(description: String = "") = importIcon(description)

  def export(description: String = "") = exportIcon(description)

  def send(description: String = "") = sendIcon(description)

  def floppyDisk(description: String = "") = floppyDiskIcon(description)

  def floppySaved(description: String = "") = floppySavedIcon(description)

  def floppyRemove(description: String = "") = floppyRemoveIcon(description)

  def floppySave(description: String = "") = floppySaveIcon(description)

  def floppyOpen(description: String = "") = floppyOpenIcon(description)

  def creditCard(description: String = "") = creditCardIcon(description)

  def transfer(description: String = "") = transferIcon(description)

  def cutlery(description: String = "") = cutleryIcon(description)

  def header(description: String = "") = headerIcon(description)

  def compressed(description: String = "") = compressedIcon(description)

  def earphone(description: String = "") = earphoneIcon(description)

  def phoneAlt(description: String = "") = phoneAltIcon(description)

  def tower(description: String = "") = towerIcon(description)

  def stats(description: String = "") = statsIcon(description)

  def sdVideo(description: String = "") = sdVideoIcon(description)

  def hdVideo(description: String = "") = hdVideoIcon(description)

  def subtitles(description: String = "") = subtitlesIcon(description)

  def soundStereo(description: String = "") = soundStereoIcon(description)

  def soundDolby(description: String = "") = soundDolbyIcon(description)

  def sound51(description: String = "") = sound51Icon(description)

  def sound61(description: String = "") = sound61Icon(description)

  def sound71(description: String = "") = sound71Icon(description)

  def copyrightMark(description: String = "") = copyrightMarkIcon(description)

  def registrationMark(description: String = "") = registrationMarkIcon(description)

  def cloudDownload(description: String = "") = cloudDownloadIcon(description)

  def cloudUpload(description: String = "") = cloudUploadIcon(description)

  def treeConifer(description: String = "") = treeConiferIcon(description)

  def treeDeciduous(description: String = "") = treeDeciduousIcon(description)

  def cd(description: String = "") = cdIcon(description)

  def saveFile(description: String = "") = saveFileIcon(description)

  def openFile(description: String = "") = openFileIcon(description)

  def levelUp(description: String = "") = levelUpIcon(description)

  def copy(description: String = "") = copyIcon(description)

  def paste(description: String = "") = pasteIcon(description)

  def alert(description: String = "") = alertIcon(description)

  def equalizer(description: String = "") = equalizerIcon(description)

  def king(description: String = "") = kingIcon(description)

  def queen(description: String = "") = queenIcon(description)

  def pawn(description: String = "") = pawnIcon(description)

  def bishop(description: String = "") = bishopIcon(description)

  def knight(description: String = "") = knightIcon(description)

  def babyFormula(description: String = "") = babyFormulaIcon(description)

  def tent(description: String = "") = tentIcon(description)

  def blackboard(description: String = "") = blackboardIcon(description)

  def bed(description: String = "") = bedIcon(description)

  def apple(description: String = "") = appleIcon(description)

  def erase(description: String = "") = eraseIcon(description)

  def hourglass(description: String = "") = hourglassIcon(description)

  def lamp(description: String = "") = lampIcon(description)

  def duplicate(description: String = "") = duplicateIcon(description)

  def piggyBank(description: String = "") = piggyBankIcon(description)

  def scissors(description: String = "") = scissorsIcon(description)

  def bitcoin(description: String = "") = bitcoinIcon(description)

  def btc(description: String = "") = btcIcon(description)

  def xbt(description: String = "") = xbtIcon(description)

  def yen(description: String = "") = yenIcon(description)

  def jpy(description: String = "") = jpyIcon(description)

  def ruble(description: String = "") = rubleIcon(description)

  def rub(description: String = "") = rubIcon(description)

  def scale(description: String = "") = scaleIcon(description)

  def iceLolly(description: String = "") = iceLollyIcon(description)

  def iceLollyTasted(description: String = "") = iceLollyTastedIcon(description)

  def education(description: String = "") = educationIcon(description)

  def optionHorizontal(description: String = "") = optionHorizontalIcon(description)

  def optionVertical(description: String = "") = optionVerticalIcon(description)

  def menuHamburger(description: String = "") = menuHamburgerIcon(description)

  def modalWindow(description: String = "") = modalWindowIcon(description)

  def oil(description: String = "") = oilIcon(description)

  def grain(description: String = "") = grainIcon(description)

  def sunglasses(description: String = "") = sunglassesIcon(description)

  def textSize(description: String = "") = textSizeIcon(description)

  def textColor(description: String = "") = textColorIcon(description)

  def textBackground(description: String = "") = textBackgroundIcon(description)

  def objectAlignTop(description: String = "") = objectAlignTopIcon(description)

  def objectAlignBottom(description: String = "") = objectAlignBottomIcon(description)

  def objectAlignHorizontal(description: String = "") = objectAlignHorizontalIcon(description)

  def objectAlignLeft(description: String = "") = objectAlignLeftIcon(description)

  def objectAlignVertical(description: String = "") = objectAlignVerticalIcon(description)

  def objectAlignRight(description: String = "") = objectAlignRightIcon(description)

  def triangleRight(description: String = "") = triangleRightIcon(description)

  def triangleLeft(description: String = "") = triangleLeftIcon(description)

  def triangleBottom(description: String = "") = triangleBottomIcon(description)

  def triangleTop(description: String = "") = triangleTopIcon(description)

  def console(description: String = "") = consoleIcon(description)

  def superscript(description: String = "") = superscriptIcon(description)

  def subscript(description: String = "") = subscriptIcon(description)

  def menuLeft(description: String = "") = menuLeftIcon(description)

  def menuRight(description: String = "") = menuRightIcon(description)

  def menuDown(description: String = "") = menuDownIcon(description)

  def menuUp(description: String = "") = menuUpIcon(description)

  case object asteriskIcon extends Icon {
    val code = "asterisk"
  }

  case object plusIcon extends Icon {
    val code = "plus"
  }

  case object euroIcon extends Icon {
    val code = "euro"
  }

  case object eurIcon extends Icon {
    val code = "eur"
  }

  case object minusIcon extends Icon {
    val code = "minus"
  }

  case object cloudIcon extends Icon {
    val code = "cloud"
  }

  case object envelopeIcon extends Icon {
    val code = "envelope"
  }

  case object pencilIcon extends Icon {
    val code = "pencil"
  }

  case object glassIcon extends Icon {
    val code = "glass"
  }

  case object musicIcon extends Icon {
    val code = "music"
  }

  case object searchIcon extends Icon {
    val code = "search"
  }

  case object heartIcon extends Icon {
    val code = "heart"
  }

  case object starIcon extends Icon {
    val code = "star"
  }

  case object starEmptyIcon extends Icon {
    val code = "star-empty"
  }

  case object userIcon extends Icon {
    val code = "user"
  }

  case object filmIcon extends Icon {
    val code = "film"
  }

  case object thLargeIcon extends Icon {
    val code = "th-large"
  }

  case object thIcon extends Icon {
    val code = "th"
  }

  case object thListIcon extends Icon {
    val code = "th-list"
  }

  case object okIcon extends Icon {
    val code = "ok"
  }

  case object removeIcon extends Icon {
    val code = "remove"
  }

  case object zoomInIcon extends Icon {
    val code = "zoom-in"
  }

  case object zoomOutIcon extends Icon {
    val code = "zoom-out"
  }

  case object offIcon extends Icon {
    val code = "off"
  }

  case object signalIcon extends Icon {
    val code = "signal"
  }

  case object cogIcon extends Icon {
    val code = "cog"
  }

  case object trashIcon extends Icon {
    val code = "trash"
  }

  case object homeIcon extends Icon {
    val code = "home"
  }

  case object fileIcon extends Icon {
    val code = "file"
  }

  case object timeIcon extends Icon {
    val code = "time"
  }

  case object roadIcon extends Icon {
    val code = "road"
  }

  case object downloadAltIcon extends Icon {
    val code = "download-alt"
  }

  case object downloadIcon extends Icon {
    val code = "download"
  }

  case object uploadIcon extends Icon {
    val code = "upload"
  }

  case object inboxIcon extends Icon {
    val code = "inbox"
  }

  case object playCircleIcon extends Icon {
    val code = "play-circle"
  }

  case object repeatIcon extends Icon {
    val code = "repeat"
  }

  case object refreshIcon extends Icon {
    val code = "refresh"
  }

  case object listAltIcon extends Icon {
    val code = "list-alt"
  }

  case object lockIcon extends Icon {
    val code = "lock"
  }

  case object flagIcon extends Icon {
    val code = "flag"
  }

  case object headphonesIcon extends Icon {
    val code = "headphones"
  }

  case object volumeOffIcon extends Icon {
    val code = "volume-off"
  }

  case object volumeDownIcon extends Icon {
    val code = "volume-down"
  }

  case object volumeUpIcon extends Icon {
    val code = "volume-up"
  }

  case object qrcodeIcon extends Icon {
    val code = "qrcode"
  }

  case object barcodeIcon extends Icon {
    val code = "barcode"
  }

  case object tagIcon extends Icon {
    val code = "tag"
  }

  case object tagsIcon extends Icon {
    val code = "tags"
  }

  case object bookIcon extends Icon {
    val code = "book"
  }

  case object bookmarkIcon extends Icon {
    val code = "bookmark"
  }

  case object printIcon extends Icon {
    val code = "print"
  }

  case object cameraIcon extends Icon {
    val code = "camera"
  }

  case object fontIcon extends Icon {
    val code = "font"
  }

  case object boldIcon extends Icon {
    val code = "bold"
  }

  case object italicIcon extends Icon {
    val code = "italic"
  }

  case object textHeightIcon extends Icon {
    val code = "text-height"
  }

  case object textWidthIcon extends Icon {
    val code = "text-width"
  }

  case object alignLeftIcon extends Icon {
    val code = "align-left"
  }

  case object alignCenterIcon extends Icon {
    val code = "align-center"
  }

  case object alignRightIcon extends Icon {
    val code = "align-right"
  }

  case object alignJustifyIcon extends Icon {
    val code = "align-justify"
  }

  case object listIcon extends Icon {
    val code = "list"
  }

  case object indentLeftIcon extends Icon {
    val code = "indent-left"
  }

  case object indentRightIcon extends Icon {
    val code = "indent-right"
  }

  case object facetimeVideoIcon extends Icon {
    val code = "facetime-video"
  }

  case object pictureIcon extends Icon {
    val code = "picture"
  }

  case object mapMarkerIcon extends Icon {
    val code = "map-marker"
  }

  case object adjustIcon extends Icon {
    val code = "adjust"
  }

  case object tintIcon extends Icon {
    val code = "tint"
  }

  case object editIcon extends Icon {
    val code = "edit"
  }

  case object shareIcon extends Icon {
    val code = "share"
  }

  case object checkIcon extends Icon {
    val code = "check"
  }

  case object moveIcon extends Icon {
    val code = "move"
  }

  case object stepBackwardIcon extends Icon {
    val code = "step-backward"
  }

  case object fastBackwardIcon extends Icon {
    val code = "fast-backward"
  }

  case object backwardIcon extends Icon {
    val code = "backward"
  }

  case object playIcon extends Icon {
    val code = "play"
  }

  case object pauseIcon extends Icon {
    val code = "pause"
  }

  case object stopIcon extends Icon {
    val code = "stop"
  }

  case object forwardIcon extends Icon {
    val code = "forward"
  }

  case object fastForwardIcon extends Icon {
    val code = "fast-forward"
  }

  case object stepForwardIcon extends Icon {
    val code = "step-forward"
  }

  case object ejectIcon extends Icon {
    val code = "eject"
  }

  case object chevronLeftIcon extends Icon {
    val code = "chevron-left"
  }

  case object chevronRightIcon extends Icon {
    val code = "chevron-right"
  }

  case object plusSignIcon extends Icon {
    val code = "plus-sign"
  }

  case object minusSignIcon extends Icon {
    val code = "minus-sign"
  }

  case object removeSignIcon extends Icon {
    val code = "remove-sign"
  }

  case object okSignIcon extends Icon {
    val code = "ok-sign"
  }

  case object questionSignIcon extends Icon {
    val code = "question-sign"
  }

  case object infoSignIcon extends Icon {
    val code = "info-sign"
  }

  case object screenshotIcon extends Icon {
    val code = "screenshot"
  }

  case object removeCircleIcon extends Icon {
    val code = "remove-circle"
  }

  case object okCircleIcon extends Icon {
    val code = "ok-circle"
  }

  case object banCircleIcon extends Icon {
    val code = "ban-circle"
  }

  case object arrowLeftIcon extends Icon {
    val code = "arrow-left"
  }

  case object arrowRightIcon extends Icon {
    val code = "arrow-right"
  }

  case object arrowUpIcon extends Icon {
    val code = "arrow-up"
  }

  case object arrowDownIcon extends Icon {
    val code = "arrow-down"
  }

  case object shareAltIcon extends Icon {
    val code = "share-alt"
  }

  case object resizeFullIcon extends Icon {
    val code = "resize-full"
  }

  case object resizeSmallIcon extends Icon {
    val code = "resize-small"
  }

  case object exclamationSignIcon extends Icon {
    val code = "exclamation-sign"
  }

  case object giftIcon extends Icon {
    val code = "gift"
  }

  case object leafIcon extends Icon {
    val code = "leaf"
  }

  case object fireIcon extends Icon {
    val code = "fire"
  }

  case object eyeOpenIcon extends Icon {
    val code = "eye-open"
  }

  case object eyeCloseIcon extends Icon {
    val code = "eye-close"
  }

  case object warningSignIcon extends Icon {
    val code = "warning-sign"
  }

  case object planeIcon extends Icon {
    val code = "plane"
  }

  case object calendarIcon extends Icon {
    val code = "calendar"
  }

  case object randomIcon extends Icon {
    val code = "random"
  }

  case object commentIcon extends Icon {
    val code = "comment"
  }

  case object magnetIcon extends Icon {
    val code = "magnet"
  }

  case object chevronUpIcon extends Icon {
    val code = "chevron-up"
  }

  case object chevronDownIcon extends Icon {
    val code = "chevron-down"
  }

  case object retweetIcon extends Icon {
    val code = "retweet"
  }

  case object shoppingCartIcon extends Icon {
    val code = "shopping-cart"
  }

  case object folderCloseIcon extends Icon {
    val code = "folder-close"
  }

  case object folderOpenIcon extends Icon {
    val code = "folder-open"
  }

  case object resizeVerticalIcon extends Icon {
    val code = "resize-vertical"
  }

  case object resizeHorizontalIcon extends Icon {
    val code = "resize-horizontal"
  }

  case object hddIcon extends Icon {
    val code = "hdd"
  }

  case object bullhornIcon extends Icon {
    val code = "bullhorn"
  }

  case object bellIcon extends Icon {
    val code = "bell"
  }

  case object certificateIcon extends Icon {
    val code = "certificate"
  }

  case object thumbsUpIcon extends Icon {
    val code = "thumbs-up"
  }

  case object thumbsDownIcon extends Icon {
    val code = "thumbs-down"
  }

  case object handRightIcon extends Icon {
    val code = "hand-right"
  }

  case object handLeftIcon extends Icon {
    val code = "hand-left"
  }

  case object handUpIcon extends Icon {
    val code = "hand-up"
  }

  case object handDownIcon extends Icon {
    val code = "hand-down"
  }

  case object circleArrowRightIcon extends Icon {
    val code = "circle-arrow-right"
  }

  case object circleArrowLeftIcon extends Icon {
    val code = "circle-arrow-left"
  }

  case object circleArrowUpIcon extends Icon {
    val code = "circle-arrow-up"
  }

  case object circleArrowDownIcon extends Icon {
    val code = "circle-arrow-down"
  }

  case object globeIcon extends Icon {
    val code = "globe"
  }

  case object wrenchIcon extends Icon {
    val code = "wrench"
  }

  case object tasksIcon extends Icon {
    val code = "tasks"
  }

  case object filterIcon extends Icon {
    val code = "filter"
  }

  case object briefcaseIcon extends Icon {
    val code = "briefcase"
  }

  case object fullscreenIcon extends Icon {
    val code = "fullscreen"
  }

  case object dashboardIcon extends Icon {
    val code = "dashboard"
  }

  case object paperclipIcon extends Icon {
    val code = "paperclip"
  }

  case object heartEmptyIcon extends Icon {
    val code = "heart-empty"
  }

  case object linkIcon extends Icon {
    val code = "link"
  }

  case object phoneIcon extends Icon {
    val code = "phone"
  }

  case object pushpinIcon extends Icon {
    val code = "pushpin"
  }

  case object usdIcon extends Icon {
    val code = "usd"
  }

  case object gbpIcon extends Icon {
    val code = "gbp"
  }

  case object sortIcon extends Icon {
    val code = "sort"
  }

  case object sortByAlphabetIcon extends Icon {
    val code = "sort-by-alphabet"
  }

  case object sortByAlphabetAltIcon extends Icon {
    val code = "sort-by-alphabet-alt"
  }

  case object sortByOrderIcon extends Icon {
    val code = "sort-by-order"
  }

  case object sortByOrderAltIcon extends Icon {
    val code = "sort-by-order-alt"
  }

  case object sortByAttributesIcon extends Icon {
    val code = "sort-by-attributes"
  }

  case object sortByAttributesAltIcon extends Icon {
    val code = "sort-by-attributes-alt"
  }

  case object uncheckedIcon extends Icon {
    val code = "unchecked"
  }

  case object expandIcon extends Icon {
    val code = "expand"
  }

  case object collapseDownIcon extends Icon {
    val code = "collapse-down"
  }

  case object collapseUpIcon extends Icon {
    val code = "collapse-up"
  }

  case object logInIcon extends Icon {
    val code = "log-in"
  }

  case object flashIcon extends Icon {
    val code = "flash"
  }

  case object logOutIcon extends Icon {
    val code = "log-out"
  }

  case object newWindowIcon extends Icon {
    val code = "new-window"
  }

  case object recordIcon extends Icon {
    val code = "record"
  }

  case object saveIcon extends Icon {
    val code = "save"
  }

  case object openIcon extends Icon {
    val code = "open"
  }

  case object savedIcon extends Icon {
    val code = "saved"
  }

  case object importIcon extends Icon {
    val code = "import"
  }

  case object exportIcon extends Icon {
    val code = "export"
  }

  case object sendIcon extends Icon {
    val code = "send"
  }

  case object floppyDiskIcon extends Icon {
    val code = "floppy-disk"
  }

  case object floppySavedIcon extends Icon {
    val code = "floppy-saved"
  }

  case object floppyRemoveIcon extends Icon {
    val code = "floppy-remove"
  }

  case object floppySaveIcon extends Icon {
    val code = "floppy-save"
  }

  case object floppyOpenIcon extends Icon {
    val code = "floppy-open"
  }

  case object creditCardIcon extends Icon {
    val code = "credit-card"
  }

  case object transferIcon extends Icon {
    val code = "transfer"
  }

  case object cutleryIcon extends Icon {
    val code = "cutlery"
  }

  case object headerIcon extends Icon {
    val code = "header"
  }

  case object compressedIcon extends Icon {
    val code = "compressed"
  }

  case object earphoneIcon extends Icon {
    val code = "earphone"
  }

  case object phoneAltIcon extends Icon {
    val code = "phone-alt"
  }

  case object towerIcon extends Icon {
    val code = "tower"
  }

  case object statsIcon extends Icon {
    val code = "stats"
  }

  case object sdVideoIcon extends Icon {
    val code = "sd-video"
  }

  case object hdVideoIcon extends Icon {
    val code = "hd-video"
  }

  case object subtitlesIcon extends Icon {
    val code = "subtitles"
  }

  case object soundStereoIcon extends Icon {
    val code = "sound-stereo"
  }

  case object soundDolbyIcon extends Icon {
    val code = "sound-dolby"
  }

  case object sound51Icon extends Icon {
    val code = "sound-5-1"
  }

  case object sound61Icon extends Icon {
    val code = "sound-6-1"
  }

  case object sound71Icon extends Icon {
    val code = "sound-7-1"
  }

  case object copyrightMarkIcon extends Icon {
    val code = "copyright-mark"
  }

  case object registrationMarkIcon extends Icon {
    val code = "registration-mark"
  }

  case object cloudDownloadIcon extends Icon {
    val code = "cloud-download"
  }

  case object cloudUploadIcon extends Icon {
    val code = "cloud-upload"
  }

  case object treeConiferIcon extends Icon {
    val code = "tree-conifer"
  }

  case object treeDeciduousIcon extends Icon {
    val code = "tree-deciduous"
  }

  case object cdIcon extends Icon {
    val code = "cd"
  }

  case object saveFileIcon extends Icon {
    val code = "save-file"
  }

  case object openFileIcon extends Icon {
    val code = "open-file"
  }

  case object levelUpIcon extends Icon {
    val code = "level-up"
  }

  case object copyIcon extends Icon {
    val code = "copy"
  }

  case object pasteIcon extends Icon {
    val code = "paste"
  }

  case object alertIcon extends Icon {
    val code = "alert"
  }

  case object equalizerIcon extends Icon {
    val code = "equalizer"
  }

  case object kingIcon extends Icon {
    val code = "king"
  }

  case object queenIcon extends Icon {
    val code = "queen"
  }

  case object pawnIcon extends Icon {
    val code = "pawn"
  }

  case object bishopIcon extends Icon {
    val code = "bishop"
  }

  case object knightIcon extends Icon {
    val code = "knight"
  }

  case object babyFormulaIcon extends Icon {
    val code = "baby-formula"
  }

  case object tentIcon extends Icon {
    val code = "tent"
  }

  case object blackboardIcon extends Icon {
    val code = "blackboard"
  }

  case object bedIcon extends Icon {
    val code = "bed"
  }

  case object appleIcon extends Icon {
    val code = "apple"
  }

  case object eraseIcon extends Icon {
    val code = "erase"
  }

  case object hourglassIcon extends Icon {
    val code = "hourglass"
  }

  case object lampIcon extends Icon {
    val code = "lamp"
  }

  case object duplicateIcon extends Icon {
    val code = "duplicate"
  }

  case object piggyBankIcon extends Icon {
    val code = "piggy-bank"
  }

  case object scissorsIcon extends Icon {
    val code = "scissors"
  }

  case object bitcoinIcon extends Icon {
    val code = "bitcoin"
  }

  case object btcIcon extends Icon {
    val code = "btc"
  }

  case object xbtIcon extends Icon {
    val code = "xbt"
  }

  case object yenIcon extends Icon {
    val code = "yen"
  }

  case object jpyIcon extends Icon {
    val code = "jpy"
  }

  case object rubleIcon extends Icon {
    val code = "ruble"
  }

  case object rubIcon extends Icon {
    val code = "rub"
  }

  case object scaleIcon extends Icon {
    val code = "scale"
  }

  case object iceLollyIcon extends Icon {
    val code = "ice-lolly"
  }

  case object iceLollyTastedIcon extends Icon {
    val code = "ice-lolly-tasted"
  }

  case object educationIcon extends Icon {
    val code = "education"
  }

  case object optionHorizontalIcon extends Icon {
    val code = "option-horizontal"
  }

  case object optionVerticalIcon extends Icon {
    val code = "option-vertical"
  }

  case object menuHamburgerIcon extends Icon {
    val code = "menu-hamburger"
  }

  case object modalWindowIcon extends Icon {
    val code = "modal-window"
  }

  case object oilIcon extends Icon {
    val code = "oil"
  }

  case object grainIcon extends Icon {
    val code = "grain"
  }

  case object sunglassesIcon extends Icon {
    val code = "sunglasses"
  }

  case object textSizeIcon extends Icon {
    val code = "text-size"
  }

  case object textColorIcon extends Icon {
    val code = "text-color"
  }

  case object textBackgroundIcon extends Icon {
    val code = "text-background"
  }

  case object objectAlignTopIcon extends Icon {
    val code = "object-align-top"
  }

  case object objectAlignBottomIcon extends Icon {
    val code = "object-align-bottom"
  }

  case object objectAlignHorizontalIcon extends Icon {
    val code = "object-align-horizontal"
  }

  case object objectAlignLeftIcon extends Icon {
    val code = "object-align-left"
  }

  case object objectAlignVerticalIcon extends Icon {
    val code = "object-align-vertical"
  }

  case object objectAlignRightIcon extends Icon {
    val code = "object-align-right"
  }

  case object triangleRightIcon extends Icon {
    val code = "triangle-right"
  }

  case object triangleLeftIcon extends Icon {
    val code = "triangle-left"
  }

  case object triangleBottomIcon extends Icon {
    val code = "triangle-bottom"
  }

  case object triangleTopIcon extends Icon {
    val code = "triangle-top"
  }

  case object consoleIcon extends Icon {
    val code = "console"
  }

  case object superscriptIcon extends Icon {
    val code = "superscript"
  }

  case object subscriptIcon extends Icon {
    val code = "subscript"
  }

  case object menuLeftIcon extends Icon {
    val code = "menu-left"
  }

  case object menuRightIcon extends Icon {
    val code = "menu-right"
  }

  case object menuDownIcon extends Icon {
    val code = "menu-down"
  }

  case object menuUpIcon extends Icon {
    val code = "menu-up"
  }
}

sealed trait Icon {
  val code: String

  def apply(description: String) =
    Html( s"<span class='glyphicon glyphicon-$code' aria-hidden='true'></span><span class='sr-only'>$description</span>")

  def apply() =
    Html( s"<span class='glyphicon glyphicon-$code' aria-hidden='true'></span>")
}

