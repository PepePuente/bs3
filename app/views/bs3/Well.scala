package views.bs3

import play.twirl.api.Html

class Well extends DivTag[Well] with Sized[Well] {
  override val baseCss = "well"
  css("well")
}
