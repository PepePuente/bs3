package views.bs3

import play.twirl.api.Html

class Jumbotron extends DivTag[Jumbotron] {
  private var isFullWidth = false
  css("jumbotron")

  def fullwidth = {
    isFullWidth = true
    this
  }

  override def apply(body: Html) =
    if (isFullWidth)
      super.apply(new GridContainer().apply(body))
    else
      super.apply(body)
}