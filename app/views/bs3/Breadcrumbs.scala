package views.bs3

class Breadcrumbs extends FilledHtmlTag[Breadcrumbs] {
  override val tag = "ol"
  css("breadcrumb")
}

class Breadcrumb extends AnchoredListItemTag[Breadcrumb]

class ActiveBreadcrumb extends ListItemTag[ActiveBreadcrumb] {
  css("active")
}