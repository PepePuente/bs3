package views.bs3

import play.twirl.api.Html

class Navbar extends FilledHtmlTag[Navbar] {
  override val tag = "nav"
  css("navbar")
  private var style: NavbarStyle = NavbarDefault
  private var position: NavbarPosition = NavbarAuto
  private val container: GridContainer = new GridContainer

  def inverted = {
    style = NavbarInverse
    this
  }

  def setPosition(position: NavbarPosition) = {
    this.position = position
    this
  }

  def fixedTop = setPosition(NavbarFixedTop)

  def fixedBottom = setPosition(NavbarFixedBottom)

  def staticTop = setPosition(NavbarStaticTop)

  def fluid = {
    container.fluid
    this
  }

  override protected def prepare() = {
    super.prepare()
    css(style).css(position)
  }

  override def apply(body: Html) = super.apply(container(body))

  def x(a: Html)(b: Html): Html = new Html(a :: b :: Nil)
}

sealed trait NavbarStyle extends CssClass

object NavbarDefault extends NavbarStyle {
  val cssClass = "navbar-default"
}

object NavbarInverse extends NavbarStyle {
  val cssClass = "navbar-inverse"
}

sealed trait NavbarPosition extends CssClass

object NavbarAuto extends NavbarPosition {
  val cssClass = ""
}

object NavbarFixedTop extends NavbarPosition {
  val cssClass = "navbar-fixed-top"
}

object NavbarFixedBottom extends NavbarPosition {
  val cssClass = "navbar-fixed-bottom"
}

object NavbarStaticTop extends NavbarPosition {
  val cssClass = "navbar-static-top"
}

class NavbarBrand(id: String = "navbar-body") extends DivTag[NavbarBrand] {
  id(id)

  override protected def openTag() =
    s"""
<div class="navbar-header" id="${getAttr("id")}">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#${getAttr("id")} + div" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">"""

  override protected def closeTag() = """</a></div>"""
}

class NavbarBody extends DivTag[NavbarBody] {
  css("collapse navbar-collapse")
}
