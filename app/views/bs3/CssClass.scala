package views.bs3

trait CssClass {
  val cssClass: String
}

trait Bs3CssClass {
  protected val cssClass: String
  def cssClass(base: String): String = if (cssClass.isEmpty) "" else base + "-" + cssClass
}

sealed trait Size extends Bs3CssClass

case object Large extends Size {
  protected val cssClass = "lg"
}

case object DefaultSize extends Size {
  protected val cssClass = ""
}

case object Small extends Size {
  protected val cssClass = "sm"
}

case object ExtraSmall extends Size {
  protected val cssClass = "xs"
}

sealed trait ContextualColor extends Bs3CssClass

case object DefaultContextualColor extends ContextualColor {
  protected val cssClass = "default"
}

case object Primary extends ContextualColor {
  protected val cssClass = "primary"
}

case object Success extends ContextualColor {
  protected val cssClass = "success"
}

case object Info extends ContextualColor {
  protected val cssClass = "info"
}

case object Warning extends ContextualColor {
  protected val cssClass = "warning"
}

case object Danger extends ContextualColor {
  protected val cssClass = "danger"
}

case object LinkContextualColor extends ContextualColor {
  protected val cssClass = "link"
}

case object ActiveContextualColor extends ContextualColor {
  protected val cssClass = "active"

  override def cssClass(base: String) = cssClass
}

case object DisabledContextualColor extends ContextualColor {
  protected val cssClass = "disabled"

  override def cssClass(base: String) = cssClass
}

case object NoContextualColor extends ContextualColor {
  protected val cssClass = ""

  override def cssClass(base: String) = cssClass
}

sealed trait State extends CssClass

case object DisabledState extends State {
  val cssClass = "disabled"
}

case object ActiveState extends State {
  val cssClass = "active"
}

case object EnabledState extends State {
  val cssClass = ""
}
