package views.bs3

class Button extends FilledHtmlTag[Button] with Colored[Button] with Sized[Button] {
  override val tag = "button"
  override val baseCss = "btn"
  defaultColor.defaultSize.css(baseCss).setAttr("type", "button")

  def dropdown = new Menu(Down)

  def dropdown(id: String) = new Menu(Down).id(id)

  def dropup = new Menu(Up)

  def dropup(id: String) = new Menu(Up).id(id)
}
