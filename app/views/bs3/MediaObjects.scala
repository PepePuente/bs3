package views.bs3

class MediaObject extends DivTag[MediaObject] {
  css("media")

  def left = new MediaItem()

  def right = new MediaItem().right

  def body = new MediaBody()

  def heading = new MediaHeading(4)

  def heading(i: Int) = new MediaHeading(i)
}

class MediaList extends ListTag[MediaList] {
  css("media-list")

  def item = new MediaListItem
}


class MediaListItem extends ListItemTag[MediaListItem] {
  css("media")
}

class MediaItem extends DivTag[MediaItem] {
  private var hAlign: MediaHorizontalAlignment = MediaLeft
  private var vAlign: MediaVerticalAlignment = MediaTop

  def setHorizontalAlignment(hAlign: MediaHorizontalAlignment) = {
    this.hAlign = hAlign
    this
  }

  def left = setHorizontalAlignment(MediaLeft)

  def right = setHorizontalAlignment(MediaRight)

  def setVerticalAlignment(vAlign: MediaVerticalAlignment) = {
    this.vAlign = vAlign
    this
  }

  def top = setVerticalAlignment(MediaTop)

  def middle = setVerticalAlignment(MediaMiddle)

  def bottom = setVerticalAlignment(MediaBottom)

  override def prepare() = css(hAlign).css(vAlign)
}

class MediaBody extends DivTag[MediaBody] {
  css("media-body")
}

class MediaHeading(i: Int) extends FilledHtmlTag[MediaHeading] {
  override val tag = "h" + i
  css("media-heading")
}

sealed trait MediaVerticalAlignment extends CssClass

object MediaTop extends MediaVerticalAlignment {
  val cssClass = ""
}

object MediaMiddle extends MediaVerticalAlignment {
  val cssClass = "media-middle"
}

object MediaBottom extends MediaVerticalAlignment {
  val cssClass = "media-bottom"
}

sealed trait MediaHorizontalAlignment extends CssClass

object MediaLeft extends MediaHorizontalAlignment {
  val cssClass = "media-left"
}

object MediaRight extends MediaHorizontalAlignment {
  val cssClass = "media-right"
}
