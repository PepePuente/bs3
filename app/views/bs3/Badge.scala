package views.bs3

import play.twirl.api.Html
import views.bs3.FilledHtmlTag

class Badge extends FilledHtmlTag[Badge] {
  override val tag = "span"
  css("badge")

  def apply(txt: String): Html = apply(Html(txt))
}