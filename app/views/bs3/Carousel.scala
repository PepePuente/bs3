package views.bs3

import play.twirl.api.Html

class Carousel extends DivTag[Carousel] {
  val indicators = new CarouselIndicators(this)
  val slidesWrapper = new SimpleDivTag().css("carousel-inner").role("listbox")
  css("carousel").css("slide").setAttr("data-ride", "carousel")

  def apply(f: Carousel => Html): Html = {
    val slides = f(this)
    apply(new Html(indicators() :: slidesWrapper(new Html(slides :: left(glyphicon.chevronLeft()) :: right(glyphicon.chevronRight()) :: Nil)) :: Nil))
  }

  def slide(src: String) = {
    val n = indicators.addItem()
    val item = new CarouselSlide(src)
    if (n == 1) item.css("active")
    item
  }

  def left = new SimpleAnchorTag().css("left").css("carousel-control")
    .href("#" + id).role("button").setAttr("data-slide", "prev")

  def right = new SimpleAnchorTag().css("right").css("carousel-control")
    .href("#" + id).role("button").setAttr("data-slide", "next")
}

class CarouselSlide(src: String) extends DivTag[CarouselSlide] {
  css("item")

  val caption = new SimpleDivTag().css("carousel-caption")

  def imgTag() = Html(s"<img src='$src'>")

  override def apply(body: Html) = new Html(Html(openTag()) :: imgTag() :: caption(body) :: Html(closeTag()) :: Nil)
}

class CarouselIndicators(carousel: Carousel) extends HtmlTag[CarouselIndicators] {
  override val tag = "ol"
  private var items: List[SimpleListItemTag] = Nil
  css("carousel-indicators")

  def addItem() = {
    val li = new SimpleListItemTag()
      .setAttr("data-target", "#" + carousel.id)
      .setAttr("data-slide-to", items.length.toString)
    if (items.isEmpty) li.css("active")
    items = li :: items
    items.length
  }

  def apply() = {
    prepare()
    new Html(openTag() + items.reverse.map { it =>
      it(Html(""))
    }.mkString("") + closeTag())
  }
}
