package views.bs3

import play.twirl.api._

class MenuItem extends AnchoredListItemTag[MenuItem] with Stated[MenuItem] {
  def tab(id:String) = {
    anchor.setAttr("data-toggle", "tab").href("#" + id)
    this
  }
}

class Menu(direction: MenuDirection) extends HtmlTag[Menu] with Colored[Menu] with Sized[Menu] {
  override val tag = "div"
  private var isSplit: Boolean = false

  color(DefaultContextualColor).size(DefaultSize)

  def split = {
    isSplit = true
    this
  }

  def apply(label: Html)(body: Html): Html = {
    val outerDiv = new SimpleDivTag().css("btn-group").css(direction)

    val extraButton = if (isSplit) new SimpleBtnTag().css(color).css(size).apply(label) else Html("")

    val caret = Html(" <span class='caret'></span>")
    val buttonLabel = if (isSplit) caret else new Html(label :: caret :: Nil)
    val button = new SimpleBtnTag().id(getAttr("id")).css("dropdown-toggle").css(color).css(size)
      .setAttr("data-toggle", "dropdown").setAttr("aria-haspopup", "true").setAttr("aria-expanded", "true")

    val menu = new SimpleListTag().css("dropdown-menu").setAttr("aria-labelledby", getAttr("id"))

    outerDiv(new Html(
      extraButton :: button(buttonLabel) :: menu(body) :: Nil
    ))
  }
}

sealed trait MenuDirection extends CssClass

case object Down extends MenuDirection {
  val cssClass = "dropdown"
}

case object Up extends MenuDirection {
  val cssClass = "dropup"
}
