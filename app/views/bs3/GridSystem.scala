package views.bs3

class GridContainer extends DivTag[GridContainer] {
  css("container")

  def fluid = css("container-fluid")
}

class GridRow extends DivTag[GridRow] {
  css("row")
}

object GridColumn {
  def apply() = new GridColumn
}

class GridColumn extends DivTag[GridColumn] {
  def lg(i: Int) = css(s"col-lg-$i")

  def lgoffset(i: Int) = css(s"col-lg-offset-$i")

  def lgpull(i: Int) = css(s"col-lg-pull-$i")

  def lgpush(i: Int) = css(s"col-lg-push-$i")

  def md(i: Int) = css(s"col-md-$i")

  def mdoffset(i: Int) = css(s"col-md-offset-$i")

  def mdpull(i: Int) = css(s"col-md-pull-$i")

  def mdpush(i: Int) = css(s"col-md-push-$i")

  def sm(i: Int) = css(s"col-sm-$i")

  def smoffset(i: Int) = css(s"col-sm-offset-$i")

  def smpull(i: Int) = css(s"col-sm-pull-$i")

  def smpush(i: Int) = css(s"col-sm-push-$i")

  def xs(i: Int) = css(s"col-xs-$i")

  def xsoffset(i: Int) = css(s"col-xs-offset-$i")

  def xspull(i: Int) = css(s"col-xs-pull-$i")

  def xspush(i: Int) = css(s"col-xs-push-$i")
}

object ClearColumn {
  def apply() = new ClearColumn
}

class ClearColumn extends EmptyHtmlTag[ClearColumn] {
  override val tag = "div"
  css("clearfix")

  def lg = css("visible-lg-block")

  def md = css("visible-md-block")

  def sm = css("visible-sm-block")

  def xs = css("visible-xs-block")
}
