package views.bs3

class Pagination extends ListTag[Pagination] with Sized[Pagination] {
  override val baseCss = "pagination"
  css("pagination")

  override def openTag() = "<nav>" + super.openTag()

  override def closeTag() = super.closeTag() + "</nav>"
}

class Page extends AnchoredListItemTag[Page] with Stated[Page] {
  state(EnabledState)

  override def closeTag() = spanCurrent + super.closeTag()

  private def spanCurrent = if (state == ActiveState) "<span class='sr-only'>(current)</span>" else ""
}

sealed trait PageType

case object PreviousPage extends PageType

case object NormalPage extends PageType

case object NextPage extends PageType

class Pager extends ListTag[Pager] {
  css("pager")

  override def openTag() = "<nav>" + super.openTag()

  override def closeTag() = super.closeTag() + "</nav>"
}

class PagerButton extends AnchoredListItemTag[PagerButton] with Stated[PagerButton]

class PagerPrevious extends AnchoredListItemTag[PagerPrevious] with Stated[PagerPrevious] {
  css("previous")
}

class PagerNext extends AnchoredListItemTag[PagerNext] with Stated[PagerNext] {
  css("next")
}