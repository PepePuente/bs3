package views.bs3

object Label {
  def default = new Label(DefaultContextualColor)

  def primary = new Label(Primary)

  def success = new Label(Success)

  def info = new Label(Info)

  def warning = new Label(Warning)

  def danger = new Label(Danger)
}

class Label(color: ContextualColor) extends FilledHtmlTag[Label] {
  override val tag = "span"
  override val baseCss = "label"
  role("alert").css("label").css(color)
}
