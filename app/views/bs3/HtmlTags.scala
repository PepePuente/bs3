package views.bs3

import play.twirl.api.{HtmlFormat, Html}

import scala.collection.immutable.Seq
import scala.collection.mutable

trait HtmlTag[T <: HtmlTag[T]] {
  this: T =>
  val tag: String
  val baseCss: String = ""
  private val attrs: HtmlAttrs = new HtmlAttrs()

  protected def prepare(): Unit = {}

  protected def openTag(): String = s"<$tag$attrs>"

  protected def closeTag(): String = s"</$tag>"

  def addAttr(name: String, value: String): T = {
    attrs.add(name, value)
    this
  }

  def setAttr(name: String, value: String): T = {
    attrs.set(name, value)
    this
  }

  protected def getAttr(name: String): String = attrs.get(name)

  def id(id: String): T = setAttr("id", id)

  def id: String = getAttr("id")

  def role(role: String): T = setAttr("role", role)

  def css(css: String): T = addAttr("class", css)

  def css(css: CssClass): T = addAttr("class", css.cssClass)

  def css(css: Bs3CssClass): T = addAttr("class", css.cssClass(baseCss))

  def style(style: String): T = addAttr("style", style)

  def collapse(target: String): T =
    setAttr("data-toggle", "collapse").setAttr("data-target", "#" + target)

  def collapse(parent: String, target: String): T =
    setAttr("data-parent", "#" + parent).collapse(target)

  def collapsed: T = css("collapse")

  def uncollapsed: T = css("in")

  def tooltip(tip: String): T =
    setAttr("data-toggle", "tooltip")
      .setAttr("title", escape(tip))

  def tooltip(tip: Html): T =
    setAttr("data-html", "true")
      .tooltip(tip.toString())

  def tooltipAt(position: String) = setAttr("data-placement", position)

  def tooltipLeft(tip: String) = tooltip(tip).tooltipAt("left")

  def tooltipTop(tip: String) = tooltip(tip).tooltipAt("top")

  def tooltipRight(tip: String) = tooltip(tip).tooltipAt("right")

  def tooltipBottom(tip: String) = tooltip(tip).tooltipAt("bottom")

  def tooltipLeft(tip: Html) = tooltip(tip).tooltipAt("left")

  def tooltipTop(tip: Html) = tooltip(tip).tooltipAt("top")

  def tooltipRight(tip: Html) = tooltip(tip).tooltipAt("right")

  def tooltipBottom(tip: Html) = tooltip(tip).tooltipAt("bottom")

  def popover(title: String, contents: String): T =
    setAttr("data-toggle", "popover")
      .setAttr("title", escape(title))
      .setAttr("data-content", escape(contents))

  def popover(title: Html)(contents: Html): T =
    setAttr("data-html", "true")
      .popover(title.toString(), contents.toString())

  def popoverAt(position: String) = addAttr("data-placement", position)

  def popoverAuto = addAttr("data-placement", "auto")

  def popoverLeft(title: String, contents: String) = popoverAt("left").popover(title, contents)

  def popoverTop(title: String, contents: String) = popoverAt("top").popover(title, contents)

  def popoverRight(title: String, contents: String) = popoverAt("right").popover(title, contents)

  def popoverBottom(title: String, contents: String) = popoverAt("bottom").popover(title, contents)

  def popoverLeft(title: Html)(contents: Html) = popoverAt("left").popover(title)(contents)

  def popoverTop(title: Html)(contents: Html) = popoverAt("top").popover(title)(contents)

  def popoverRight(title: Html)(contents: Html) = popoverAt("right").popover(title)(contents)

  def popoverBottom(title: Html)(contents: Html) = popoverAt("bottom").popover(title)(contents)

  private def escape(s: String) = s.replaceAll("'", "&apos;")
}

trait EmptyHtmlTag[T <: EmptyHtmlTag[T]] extends HtmlTag[T] {
  this: T =>
  def apply(): Html = {
    prepare()
    new Html(openTag() + closeTag())
  }
}

trait FilledHtmlTag[T <: FilledHtmlTag[T]] extends HtmlTag[T] {
  this: T =>
  def apply(body: Html): Html = {
    prepare()
    new Html(Seq(Html(openTag()), body, Html(closeTag())))
  }
}

trait DivTag[T <: DivTag[T]] extends FilledHtmlTag[T] {
  this: T =>
  override val tag = "div"
}

trait ListTag[T <: ListTag[T]] extends FilledHtmlTag[T] {
  this: T =>
  override val tag = "ul"
}

trait ListItemTag[T <: ListItemTag[T]] extends FilledHtmlTag[T] {
  this: T =>
  override val tag = "li"
}

trait NavTag[T <: NavTag[T]] extends FilledHtmlTag[T] {
  this: T =>
  override val tag = "nav"
}

trait AnchoredTag[T <: AnchoredTag[T]] extends FilledHtmlTag[T] {
  this: T =>
  override val tag = "a"
  private var aria = ""
  href("#")

  def href(href: String) = setAttr("href", href)

  def aria(aria: String): T = {
    this.aria = aria
    this
  }

  abstract override protected def prepare() = {
    super.prepare()
    if (!aria.isEmpty) setAttr("aria-label", aria)
  }

  override def openTag() = super.openTag() + ariaOpenTag

  private def ariaOpenTag = if (aria.isEmpty) "" else "<span aria-hidden='true'>"

  override def closeTag() = ariaCloseTag + super.closeTag()

  private def ariaCloseTag = if (aria.isEmpty) "" else "</span'>"
}

class SimpleAnchorTag extends AnchoredTag[SimpleAnchorTag]

class SimpleDivTag extends DivTag[SimpleDivTag]

class SimpleListTag extends FilledHtmlTag[SimpleListTag] {
  override val tag = "ul"
}

class SimpleListItemTag extends ListItemTag[SimpleListItemTag]

class SimpleButtonTag extends FilledHtmlTag[SimpleButtonTag] {
  override val tag = "button"
  setAttr("type", "button")
}

class SimpleBtnTag extends SimpleButtonTag {
  override val baseCss = "btn"
  css("btn")
}

trait AnchoredListItemTag[T <: AnchoredListItemTag[T]] extends ListItemTag[T] {
  this: T =>
  protected val anchor = new SimpleAnchorTag

  def href(href: String) = {
    anchor.href(href)
    this
  }

  def aria(aria: String) = {
    anchor.aria(aria)
    this
  }

  override def openTag() = super.openTag() + anchor.openTag()

  override def closeTag() = anchor.closeTag() + super.closeTag()
}

trait Sized[H <: Sized[H]] extends HtmlTag[H] {
  this: H =>

  protected var size: Size = DefaultSize

  def size(size: Size): H = {
    this.size = size
    this
  }

  def large = size(Large)

  def defaultSize = size(DefaultSize)

  def small = size(Small)

  def extraSmall = size(ExtraSmall)

  abstract override protected def prepare() = {
    super.prepare()
    css(size)
  }
}

trait Colored[H <: Colored[H]] extends HtmlTag[H] {
  this: H =>

  protected var color: ContextualColor = NoContextualColor

  def color(color: ContextualColor): H = {
    this.color = color
    this
  }

  def noColor = color(NoContextualColor)

  def defaultColor = color(DefaultContextualColor)

  def primary = color(Primary)

  def success = color(Success)

  def info = color(Info)

  def warning = color(Warning)

  def danger = color(Danger)

  def link = color(LinkContextualColor)

  def active = color(ActiveContextualColor)

  def disabled = color(DisabledContextualColor)

  abstract override protected def prepare() = {
    super.prepare()
    css(color)
  }
}

trait Stated[H <: Stated[H]] extends HtmlTag[H] {
  this: H =>

  protected var state: State = EnabledState

  def state(state: State): H = {
    this.state = state
    this
  }

  def getState = state

  def enabled = state(EnabledState)

  def disabled = state(DisabledState)

  def active = state(ActiveState)

  abstract override protected def prepare() = {
    super.prepare()
    css(state)
  }
}

class HtmlAttrs(attrs: mutable.Map[String, HtmlAttr] = mutable.Map()) {
  def set(name: String, value: String) = attrs(name) = new HtmlAttr(name, value)

  def get(name: String): String = if (attrs.contains(name)) attrs(name).get() else ""

  def add(name: String, value: String) =
    if (attrs.contains(name))
      attrs(name).add(value)
    else
      attrs(name) = new HtmlAttr(name, value)

  override def toString =
    if (attrs.isEmpty)
      ""
    else
      attrs.values.mkString
}

class HtmlAttr(name: String, private var value: String = "") {
  def add(newValue: String) =
    value = if (value.isEmpty)
      newValue
    else
      value + " " + newValue

  def set(newValue: String) = value = newValue

  def get() = value

  override def toString =
    if (value.isEmpty)
      ""
    else
      s" $name='$value'"
}
