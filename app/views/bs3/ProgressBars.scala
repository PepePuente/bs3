package views.bs3

import play.twirl.api.Html

class ProgressBar extends HtmlTag[ProgressBar] {
  override val tag = "div"
  private val bar = new Bar()
  css("progress")

  def showingValue = {
    bar.showingValue
    this
  }

  def setEffect(effect: BarEffect) = {
    bar.setEffect(effect)
    this
  }

  def striped = setEffect(StripedBar)

  def animated = setEffect(AnimatedBar)

  def setColor(color: ContextualColor) = {
    bar.setColor(color)
    this
  }

  def success = setColor(Success)

  def info = setColor(Info)

  def warning = setColor(Warning)

  def danger = setColor(Danger)

  def apply(value: Int) =
    new Html(openTag() + bar(value) + closeTag())
}

class StackedProgressBar extends DivTag[StackedProgressBar] {
  css("progress")
}

class Bar extends HtmlTag[Bar] {
  override val tag = "div"
  override val baseCss = "progress-bar"
  private var isValueVisible = false
  private var effect: BarEffect = SimpleBar
  private var color: ContextualColor = DefaultContextualColor

  role("progressbar").css("progress-bar")
  setAttr("aria-valuemin", "0").setAttr("aria-valuemax", "100")

  def showingValue = {
    isValueVisible = true
    this
  }

  def setEffect(effect: BarEffect) = {
    this.effect = effect
    this
  }

  def striped = setEffect(StripedBar)

  def animated = setEffect(AnimatedBar)

  def setColor(color: ContextualColor) = {
    this.color = color
    this
  }

  def success = setColor(Success)

  def info = setColor(Info)

  def warning = setColor(Warning)

  def danger = setColor(Danger)

  def apply(value: Int) = {
    css(effect).css(color)
    setAttr("aria-valuenow", s"$value")
    style(s"width: $value%;")

    if (isValueVisible) {
      style("min-width: 2em;")
      new Html(openTag() + s"$value%" + closeTag())
    } else
      new Html(openTag() + s"<span class='sr-only'>$value% Complete</span>" + closeTag())
  }
}

sealed trait BarEffect extends CssClass

case object SimpleBar extends BarEffect {
  val cssClass = ""
}

case object StripedBar extends BarEffect {
  val cssClass = "progress-bar-striped"
}

case object AnimatedBar extends BarEffect {
  val cssClass = "progress-bar-striped active"
}