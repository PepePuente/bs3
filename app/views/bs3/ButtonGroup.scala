package views.bs3

class ButtonGroup extends DivTag[ButtonGroup] with Sized[ButtonGroup] {
  override val baseCss = "btn-group"
  private var orientation: BtnGroupDirection = Horizontal

  def setOrientation(orientation: BtnGroupDirection) = {
    this.orientation = orientation
    this
  }

  def horizontal = setOrientation(Horizontal)

  def vertical = setOrientation(Vertical)

  override protected def prepare() = {
    super.prepare()
    css(orientation)
  }
}

class ButtonToolbar extends DivTag[ButtonToolbar] {
  role("toolbar").css("btn-toolbar")

  def ariaLabel(ariaLabel: String) = setAttr("aria-label", ariaLabel)
}

sealed trait BtnGroupDirection extends CssClass

case object Horizontal extends BtnGroupDirection {
  val cssClass = "btn-group"
}

case object Vertical extends BtnGroupDirection {
  val cssClass = "btn-group-vertical"
}
