package views.bs3

import play.twirl.api.Html

class Tabs extends ListTag[Tabs] {
  css("nav nav-tabs")

  private var isJustified = false

  def justified = {
    isJustified = true
    this
  }

  override protected def prepare() = {
    super.prepare()
    if (isJustified) css("nav-justified")
  }
}

class Pills extends ListTag[Pills] {
  css("nav nav-pills")

  private var isJustified = false
  private var isStacked = false

  def justified = {
    isJustified = true
    this
  }

  def stacked = {
    isStacked = true
    this
  }

  override protected def prepare() = {
    super.prepare()
    if (isJustified) css("nav-justified")
    if (isStacked) css("nav-stacked")
  }
}

class NavsItem(role: String) extends AnchoredListItemTag[NavsItem] with Stated[NavsItem] {
  role("presentation")
  anchor.role(role).setAttr("data-toggle", role)

  def tab(id:String) = {
    anchor.setAttr("data-toggle", "tab").href("#" + id)
    this
  }
}

class NavsMenu(direction: MenuDirection) extends HtmlTag[NavsMenu] {
  override val tag = "li"
  private var isSplit: Boolean = false

  def split = {
    isSplit = true
    this
  }

  def apply(label: Html)(body: Html): Html = {
    val outerItem = new SimpleListItemTag().role("presentation").css(direction)

    val extraAnchor = if (isSplit) new SimpleAnchorTag().apply(label) else Html("")

    val caret = Html(" <span class='caret'></span>")
    val anchorLabel = if (isSplit) caret else new Html(label :: caret :: Nil)
    val anchor = new SimpleAnchorTag().id(getAttr("id")).role("button").css("dropdown-toggle")
      .setAttr("data-toggle", "dropdown").setAttr("aria-haspopup", "true").setAttr("aria-expanded", "true")

    val menu = new SimpleListTag().css("dropdown-menu").setAttr("aria-labelledby", getAttr("id"))

    outerItem(new Html(
      extraAnchor :: anchor(anchorLabel) :: menu(body) :: Nil
    ))
  }
}

class TabPanes extends DivTag[TabPanes] {
  css("tab-content")
}

class TabPane extends DivTag[TabPane] {
  role("tabpanel").css("tab-pane")

  def active = css("active in")

  def fade = css("fade")
}
