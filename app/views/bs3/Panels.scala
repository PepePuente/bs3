package views.bs3

import play.twirl.api.Html

class Panel extends DivTag[Panel] with Colored[Panel] {
  override val baseCss = "panel"
  defaultColor.css("panel")
}

class PanelHeading extends DivTag[PanelHeading] {
  css("panel-heading")
}

class PanelBody extends DivTag[PanelBody] {
  css("panel-body")
}

class PanelFooter extends DivTag[PanelFooter] {
  css("panel-footer")
}

object PanelH {
  def apply(i: Int) = new PanelH(i)
}

class PanelH(i: Int) extends FilledHtmlTag[PanelH] {
  override val tag = "h" + i
  css("panel-title")
}

class AccordionPanelGroup extends DivTag[AccordionPanelGroup] {
  css("panel-group").setAttr("role", "tablist").setAttr("aria-multiselectable", "true")
}

class AccordionPanel extends HtmlTag[AccordionPanel] with Colored[AccordionPanel] {
  override val tag = "div"
  css("panel")

  css("panel-heading").role("tab")

  override def color(color: ContextualColor) = {
    panel.color(color)
    this
  }

  private val panel = new Panel

  private val heading = new PanelHeading().role("tab")
  private val anchor = new SimpleAnchorTag().role("button").css("panel-title")
    .setAttr("data-toggle", "collapse").setAttr("aria-expanded", "true")

  private val panelDiv = new SimpleDivTag().css("panel-collapse collapse").role("tabpanel")
  private val panelBody = new PanelBody

  def parent(parent: String) = {
    anchor.setAttr("data-parent", "#" + parent)
    this
  }

  override def id(id: String) = {
    anchor.href("#" + id).setAttr("aria-controls", id)
    panelDiv.id(id)
    this
  }

  def apply(head: Html)(body: Html) = panel(new Html(
    heading(anchor(head)) :: panelDiv(panelBody(body)) :: Nil
  ))
}
