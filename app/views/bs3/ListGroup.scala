package views.bs3

object ListGroup {
  def block = new DivListGroup

  def list = new ListGroup()

  def item = new ListGroupItem

  def btn = new ListGroupButton

  def link = new ListGroupLink
}

class DivListGroup extends DivTag[DivListGroup] {
  css("list-group")
}

class ListGroup extends ListTag[ListGroup] {
  css("list-group")
}

class ListGroupItem extends ListItemTag[ListGroupItem] with Colored[ListGroupItem] {
  override val baseCss = "list-group-item"
  css("list-group-item")
}

class ListGroupButton extends FilledHtmlTag[ListGroupButton] with Colored[ListGroupButton] {
  override val tag = "button"
  override val baseCss = "list-group-item"
  css("list-group-item").setAttr("type", "button")
}

class ListGroupLink extends AnchoredTag[ListGroupLink] with Colored[ListGroupLink] {
  override val baseCss = "list-group-item"
  css("list-group-item")
}
