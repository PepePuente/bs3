package views.bs3

import play.twirl.api.Html

object bs3 {

  //GRID SYSTEM
  object grid {
    def container = new GridContainer

    def row = new GridRow

    object col {
      def lg(i: Int) = GridColumn().lg(i)

      def lgoffset(i: Int) = GridColumn().lgoffset(i)

      def lgpull(i: Int) = GridColumn().lgpull(i)

      def lgpush(i: Int) = GridColumn().lgpush(i)

      def md(i: Int) = GridColumn().md(i)

      def mdoffset(i: Int) = GridColumn().mdoffset(i)

      def mdpull(i: Int) = GridColumn().mdpull(i)

      def mdpush(i: Int) = GridColumn().mdpush(i)

      def sm(i: Int) = GridColumn().sm(i)

      def smoffset(i: Int) = GridColumn().smoffset(i)

      def smpull(i: Int) = GridColumn().smpull(i)

      def smpush(i: Int) = GridColumn().smpush(i)

      def xs(i: Int) = GridColumn().xs(i)

      def xsoffset(i: Int) = GridColumn().xsoffset(i)

      def xspull(i: Int) = GridColumn().xspull(i)

      def xspush(i: Int) = GridColumn().xspush(i)

      def clear = ClearColumn()
    }

  }

  //ANCHOR
  def anchor = new SimpleAnchorTag

  //BUTTON
  def btn = new Button()

  def btnGroup = new ButtonGroup()

  def btnToolbar = new ButtonToolbar()

  // MENU
  object menu {
    def header(txt: String): Html = Html("<li class='dropdown-header'>" + txt + "</li>")

    def header(body: Html): Html = new Html(Html("<li class='dropdown-header'>") :: body :: Html("</li>") :: Nil)

    def divider = Html("<li role='separator' class='divider'></li>")

    def item = new MenuItem()
  }

  //NAVS
  def tabs = new Tabs()

  def pills = new Pills()

  object tab {
    def item = new NavsItem("tab")

    def dropdown = new NavsMenu(Down)

    def dropup = new NavsMenu(Up)

    def menuitem = new MenuItem()

    def panes = new TabPanes

    def pane(id: String) = new TabPane().id(id)
  }

  object pill {
    def item = new NavsItem("pill")

    def dropdown = new NavsMenu(Down)

    def dropup = new NavsMenu(Up)
  }

  //NAVBARS
  object navbar {
    def block = new Navbar

    def fluid = new Navbar().fluid

    def brand = new NavbarBrand

    def body = new NavbarBody
  }

  //BREADCRUMBS
  object bread {
    def crumbs = new Breadcrumbs()

    def crumb = new Breadcrumb()

    def crumb(href: String) = new Breadcrumb().href(href)

    def activecrumb = new ActiveBreadcrumb()
  }

  //PAGINATION
  def pagination = new Pagination

  def page = new Page

  object pager {
    def apply(body: Html) = new Pager()(body)

    def button = new PagerButton

    def previous = new PagerPrevious

    def next = new PagerNext
  }

  //LABELS
  def label = Label

  //BADGES
  def badge = new Badge

  //JUMBOTRON
  def jumbotron = new Jumbotron

  //ALERTS
  def alert = Alert

  //PROGRESS BARS
  def progressbar = new ProgressBar()

  def stackedprogressbar = new StackedProgressBar()

  def bar = new Bar()

  //MEDIA OBJECT
  def media = new MediaObject()

  def medialist = new MediaList()

  //LIST GROUPS
  def listgroup = ListGroup

  //PANELS
  object panel {
    def block = new Panel

    def heading = new PanelHeading

    def h1 = PanelH(1)

    def h2 = PanelH(2)

    def h3 = PanelH(3)

    def h4 = PanelH(4)

    def h5 = PanelH(5)

    def h6 = PanelH(6)

    def body = new PanelBody

    def footer = new PanelFooter
  }

  //WELLS
  def well = new Well

  //COLLAPSE
  def collapsed(id: String)(body: Html) = new SimpleDivTag().id(id).collapsed(body)

  object accordion {
    def group = new AccordionPanelGroup

    def panel = new AccordionPanel
  }

  def carousel(id: String) = new Carousel().id(id)
}
