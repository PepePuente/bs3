package views.bs3

object Alert {
  def success = this (Success)

  def info = this (Info)

  def warning = this (Warning)

  def danger = this (Danger)

  def apply(color: ContextualColor) = new Alert(color)

  protected val dismissButton = "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"
}

class Alert(color: ContextualColor) extends DivTag[Alert] {
  override val baseCss = "alert"
  private var isDismissible = false

  role("alert").css("alert").css(color)

  def dismissable = {
    isDismissible = true
    this
  }

  override def openTag() =
    if (isDismissible)
      super.openTag() + Alert.dismissButton
    else
      super.openTag()
}
